Source: astap
Section: science
Priority: optional
Maintainer: Debian Astronomy Team <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Thorsten Alteholz <debian@alteholz.de>
Build-Depends: debhelper-compat (= 13)
	, fp-compiler
	, fp-units-misc
	, fp-units-net
	, lcl
	, lcl-qt5
	, lcl-utils
	, lazarus-src
Standards-Version: 4.7.0
Homepage: https://www.hnsky.org/astap.htm
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/debian-astro-team/astap
Vcs-Git: https://salsa.debian.org/debian-astro-team/astap.git
Testsuite: autopkgtest

Package: astap
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
	, libraw-bin
Description: astrometric (plate) solver, stacking of images, photometry and FITS viewer
 ASTAP is a free stacking and astrometric solver (plate solver) program for
 deep sky images. In works with astronomical images in the FITS format, but
 can import RAW DSLR images or XISF, PGM, PPM, TIF, PNG and JPG  images. It
 has a powerful FITS viewer and the native astrometric solver can be used
 by CCDCiel, NINA, APT or SGP imaging programs to synchronise the mount
 based on an image taken.
 .
 Main features:
 .
  - Native astrometric solver, command line compatible with PlateSolve2.
  - Stacking astronomical images including dark frame and flat field correction.
     -  Filtering of deep sky images based on HFD value and average value.
     -  Alignment using an internal star match routine,
        internal astrometric solver.
     -  Mosaic building covering large areas using the astrometric linear
        solution WCS or WCS+SIP polynomial.
     -  Background equalizing.
  - FITS viewer with swipe functionality, deep sky and star annotation,
    photometry and CCD inspector.
     -  FITS thumbnail viewer.
     -  Results can be saved to 16 bit or float (-32) FITS files.
     -  Export to  JPEG, PNG, TIFF, PFM, PPM, PGM  files.
     -  FITS header edit.
     -  FITS crop function.
     -  Automatic photometry calibration against Gaia database,
        Johnson -V or Gaia Bm
     -  CCD inspector
     -  Deepsky and Hyperleda annotation
     -  Solar object annotation using MPC ephemerides
     -  Read/writes FITS binary and reads ASCII tables.
  - Some pixel math functions and digital development process
  - Can display images and tables from a multi-extension FITS.
  - Blink routine.
